<?php

declare(strict_types=1);

namespace App\Emails\Users;

use App\Models\Project;
use App\Models\User;
use Illuminate\Mail\Mailable;

class NotifyUserInvitedToProjectEmail extends Mailable
{
    public function __construct(private Project $project, private User $invitedBy, private User $invitee)
    {
    }

    public function build(): Mailable
    {
        return $this->view('emails.users.notify_user_invited_to_project', [
            'project' => $this->project,
            'invitedBy' => $this->invitedBy,
            'invitee' => $this->invitee,
        ]);
    }
}
