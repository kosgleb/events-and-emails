<?php

declare(strict_types=1);

namespace App\Events;

use App\Models\Project;
use App\Models\User;

class UserInvitedToProjectEvent
{
    public function __construct(private Project $project, private User $invitedBy, private User $invitee)
    {
    }

    public function getProject(): Project
    {
        return $this->project;
    }

    public function getInvitedBy(): User
    {
        return $this->invitedBy;
    }

    public function getInvitee(): User
    {
        return $this->invitee;
    }
}
