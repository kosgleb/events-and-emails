<?php

declare(strict_types=1);

namespace App\Jobs\Notifications;

use App\Models\Project;
use App\Models\User;
use App\Services\EmailNotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

class NotifyUserInvitedToProjectJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;

    public function __construct(
        private int $projectId,
        private int $invitedById,
        private int $inviteeId
    ) {
        $this->onQueue('notifications');
    }

    public function handle(EmailNotificationService $notificationService): void
    {
        $project = Project::find($this->projectId);
        $invitedBy = User::find($this->invitedById);
        $invitee = User::find($this->inviteeId);

        $notificationService->notifyUserInvitedToProject($project, $invitedBy, $invitee);
    }
}
