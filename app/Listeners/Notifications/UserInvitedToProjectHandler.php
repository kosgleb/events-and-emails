<?php

declare(strict_types=1);

namespace App\Listeners\Notifications;

use App\Events\UserInvitedToProjectEvent;
use App\Jobs\Notifications\NotifyUserInvitedToProjectJob;

class UserInvitedToProjectHandler
{
    public function handle(UserInvitedToProjectEvent $event): void
    {
        \dispatch(new NotifyUserInvitedToProjectJob(
            $event->getProject()->id,
            $event->getInvitedBy()->id,
            $event->getInvitee()->id
        ));
    }
}
