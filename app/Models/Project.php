<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Project
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property int $owner_id
 *
 * @property-read User $owner
 */
class Project extends Model
{
    use HasFactory;

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id', 'id', 'owner');
    }
}
