<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class ProjectUser
 * @package App\Models
 *
 * @property int $project_id
 * @property int $user_id
 */
class ProjectUser extends Pivot
{
    public $timestamps = false;
}
