<?php

declare(strict_types=1);

namespace App\Repository;

use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\User;

class ProjectUserRepository
{
    public function userInProject(User $user, Project $project): bool
    {
        return ProjectUser::query()
            ->where('project_id', $project->id)
            ->where('user_id', $user->id)
            ->exists();
    }

    public function addUserToProject(User $user, Project $project): ProjectUser
    {
        $pivot = new ProjectUser();

        $pivot->project_id = $project->id;
        $pivot->user_id = $user->id;

        $pivot->save();

        return $pivot;
    }
}
