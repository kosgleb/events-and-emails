<?php

declare(strict_types=1);

namespace App\Services;

use App\Emails\Users\NotifyUserInvitedToProjectEmail;
use App\Models\Project;
use App\Models\User;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Mail\Mailable;

class EmailNotificationService
{
    public function __construct(private Mailer $mailer, private Config $config)
    {
    }

    public function notifyUserInvitedToProject(Project $project, User $invitedBy, User $invitee): void
    {
        $message = new NotifyUserInvitedToProjectEmail($project, $invitedBy, $invitee);

        $this->sendEmail(
            $invitee->email,
            $message
        );
    }

    private function sendEmail(string $email, Mailable $message): void
    {
        $this->mailer->send(
            $message
                ->to($email)
                ->from($this->config->get('mail.from'))
        );
    }
}
