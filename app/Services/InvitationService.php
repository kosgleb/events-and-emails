<?php

declare(strict_types=1);

namespace App\Services;

use App\Events\UserInvitedToProjectEvent;
use App\Models\Project;
use App\Models\User;
use App\Repository\ProjectUserRepository;
use Illuminate\Contracts\Events\Dispatcher as EventDispatcher;

class InvitationService
{
    public function __construct(private EventDispatcher $dispatcher, private ProjectUserRepository $repository)
    {
    }

    public function invite(Project $project, User $invitedBy, User $invitee): void
    {
        // Пользователь не может пригласить сам себя, владелец проекта не может быть приглашен в него
        if ($invitee->id === $invitedBy->id || $invitee->id === $project->owner_id) {
            return;
        }

        if ($this->repository->userInProject($invitee, $project)) {
            return;
        }

        $this->repository->addUserToProject($invitee, $project);

        $this->dispatcher->dispatch(new UserInvitedToProjectEvent($project, $invitedBy, $invitee));
    }
}
