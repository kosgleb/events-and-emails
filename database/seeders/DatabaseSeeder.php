<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        /** @var User $user */
        foreach (User::all() as $user) {
            Project::factory(['owner_id' => $user->id])->create();
        }
    }
}
