<?php

declare(strict_types=1);

namespace Tests\Feature\Jobs\Notifications;

use App\Emails\Users\NotifyUserInvitedToProjectEmail;
use App\Jobs\Notifications\NotifyUserInvitedToProjectJob;
use App\Models\Project;
use App\Models\User;
use App\Services\EmailNotificationService;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class NotifyUserInvitedToProjectJobTest extends TestCase
{
    public function testItWorks(): void
    {
        Mail::fake();

        $user1 = User::factory()->create();
        $user2 = User::factory()->create();

        $project = Project::factory()->create(['owner_id' => $user1->id]);

        $job = new NotifyUserInvitedToProjectJob($project->id, $user1->id, $user2->id);

        $job->handle(
            $this->app->get(EmailNotificationService::class)
        );

        Mail::assertSent(NotifyUserInvitedToProjectEmail::class, function (NotifyUserInvitedToProjectEmail $email) use ($project, $user1, $user2) {
            return $email->hasTo($user2->email);
        });
    }
}
