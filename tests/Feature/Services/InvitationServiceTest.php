<?php

declare(strict_types=1);

namespace Tests\Feature\Services;

use App\Events\UserInvitedToProjectEvent;
use App\Models\Project;
use App\Models\User;
use App\Services\InvitationService;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class InvitationServiceTest extends TestCase
{
    public function testItWorks(): void
    {
        Event::fake();

        $user1 = User::factory()->create();
        $user2 = User::factory()->create();

        $project = Project::factory()->create(['owner_id' => $user1->id]);

        /** @var InvitationService $service */
        $service = $this->app->get(InvitationService::class);

        $service->invite($project, $user1, $user2);

        $this->assertDatabaseHas('project_user', [
            'project_id' => $project->id,
            'user_id' => $user2->id,
        ]);

        Event::assertDispatched(UserInvitedToProjectEvent::class, function (UserInvitedToProjectEvent $event) use ($project) {
            return $event->getProject()->id === $project->id;
        });
    }
}
